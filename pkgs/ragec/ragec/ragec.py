#! /usr/bin/env python3

from __future__ import absolute_import, division, print_function, unicode_literals

import click
import kubernetes
import tempfile
import time

from octodns.cmds.args import ArgumentParser
from octodns.manager import Manager

from logging import DEBUG, INFO, WARN, Formatter, StreamHandler, getLogger
from logging.handlers import SysLogHandler
from sys import stderr

from jinja2 import Template


config_template = """
---

manager:
  max_workers: 4

providers:
  yaml:
    class: octodns.provider.yaml.YamlProvider
    directory: {{ zones_dir }}
    default_ttl: 18000 # 5 hours
    enforce_order: False
  cloudflare:
    class: octodns.provider.cloudflare.CloudflareProvider
    token: env/CF_TOKEN

zones:
  prologin.dev.:
    sources:
      - yaml
    targets:
      - cloudflare
"""


prologin_dev_zone_template = """
---

## Finale 2021

ide:
  type: A
  value: 91.243.117.199
  octodns:
    cloudflare:
      proxied: true
{% for record in records %}
'{{ record }}':
  type: A
  value: 91.243.117.199
  octodns:
    cloudflare:
      proxied: true
{% endfor %}
'*.ide':
  type: A
  value: 91.243.117.199

minecraft:
  type: A
  value: 91.243.117.199

maps:
  type: A
  value: 91.243.117.199
  octodns:
    cloudflare:
      proxied: true

finale:
  type: A
  value: 91.243.117.199
  octodns:
    cloudflare:
      proxied: true
'*.finale':
  type: A
  value: 91.243.117.199

k8s:
  type: A
  value: 91.243.117.199
  octodns:
    cloudflare:
      proxied: true
grafana.k8s:
  type: A
  value: 91.243.117.199
  octodns:
    cloudflare:
      proxied: true
prometheus.k8s:
  type: A
  value: 91.243.117.199
  octodns:
    cloudflare:
      proxied: true
kibana.k8s:
  type: A
  value: 91.243.117.199
  octodns:
    cloudflare:
      proxied: true
ops.k8s:
  type: A
  value: 91.243.117.199
  octodns:
    cloudflare:
      proxied: true
web.k8s:
  type: A
  value: 91.243.117.199
  octodns:
    cloudflare:
      proxied: true
'*.k8s':
  type: A
  value: 91.243.117.199

masternode.cluster:
  type: A
  value: 10.224.33.241

champions.finale:
  type: A
  value: 10.224.33.245

registry:
  type: A
  value: 10.224.33.245

# Others

'':
  type: ALIAS
  value: prologin.org.
archives:
  type: CNAME
  value: local.prologin.dev.
bfor:
  type: CNAME
  value: local.prologin.dev.
ldap:
  type: CNAME
  value: local.prologin.dev.
local:
  type: A
  value: 163.5.5.1
'*.local':
  type: CNAME
  value: local.prologin.dev.
static:
  type: CNAME
  value: local.prologin.dev.
status:
  type: CNAME
  value: local.prologin.dev.
unicorn:
  type: CNAME
  value: local.prologin.dev.
vouch:
  type: CNAME
  value: local.prologin.dev.
"""


def _setup_logging():
    fmt = '%(asctime)s [%(thread)d] %(levelname)-5s %(name)s %(message)s'
    formatter = Formatter(fmt=fmt, datefmt='%Y-%m-%dT%H:%M:%S ')
    handler = StreamHandler(stream=stderr)
    handler.setFormatter(formatter)
    logger = getLogger()
    logger.addHandler(handler)

    logger.level = INFO

    # boto is noisy, set it to warn
    getLogger('botocore').level = WARN
    # DynectSession is noisy too
    getLogger('DynectSession').level = WARN


@click.command()
@click.option(
    "-n",
    "--dry-run",
    default=False,
    help="Only show changes, do not apply",
    show_default=True,
    is_flag=True,
)
def cli(dry_run):
    kubernetes.config.load_kube_config()
    v1 = kubernetes.client.CoreV1Api()
    api_instance = kubernetes.client.ExtensionsV1beta1Api(v1.api_client)

    all_ingresses = api_instance.list_ingress_for_all_namespaces().items
    hosts = []
    for ingress in all_ingresses:
        if "prologin.org/ragec" in ingress.metadata.annotations and ingress.metadata.annotations["prologin.org/ragec"] == "true":
            hosts.extend([rule.host for rule in ingress.spec.rules])

    hosts = [host[:-13] for host in hosts if host.endswith(".prologin.dev")]
    _setup_logging()

    zones_dir = tempfile.mkdtemp()
    with open(zones_dir + "/prologin.dev.yaml", "w") as prologin_dev_zone_file:
        prologin_dev_zone_file.write(
            Template(prologin_dev_zone_template).render(records=hosts)
        )

    config = Template(config_template).render(zones_dir=zones_dir)
    with tempfile.NamedTemporaryFile(mode="w") as config_file:
        config_file.write(config)
        config_file.file.flush()

        manager = Manager(config_file.name)
        manager.sync(
            eligible_zones=[],
            eligible_sources=[],
            eligible_targets=[],
            dry_run=dry_run,
            force=True
        )

if __name__ == "__main__":
    cli()
