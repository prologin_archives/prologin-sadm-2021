final: prev: {
  calico = prev.callPackage ./calico { };
  calicoctl = prev.callPackage ./calico/calicoctl.nix { };

  ragec = prev.callPackage ./ragec { };
}
