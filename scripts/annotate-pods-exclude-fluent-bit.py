#!/usr/bin/env python3
import json
import sys

lst = json.load(sys.stdin)

for po in lst["items"]:
    po["metadata"].setdefault(
        "annotations",
        {},
    )["fluentbit.io/exclude"] = "true"

print(json.dumps(lst))

