#! /bin/sh
# vim: tw=2000

set -eu

# set the workernode default values for config
export MASTERNODE_HOST=${MASTERNODE_HOST?Please set MASTERNODE_HOST}
export MASTERNODE_PORT=${MASTERNODE_PORT:-8067}
export MASTERNODE_HEARTBEAT_SECS=${MASTERNODE_HEARTBEAT_SECS:-5}
export MASTERNODE_MAX_RETRIES=${MASTERNODE_MAX_RETRIES:-15}
export MASTERNODE_RETRY_DELAY=${MASTERNODE_RETRY_DELAY:-10}
export MASTERNODE_SHARED_SECRET=${MASTERNODE_SHARED_SECRET?Please set MASTERNODE_SHARED_SECRET}
export WORKERNODE_PORT=${WORKERNODE_PORT:-8068}
export WORKERNODE_AVAILABLE_SLOTS=${WORKERNODE_AVAILABLE_SLOTS:-20}
export PATH_RULES=${PATH_RULES?Please set PATH_RULES}
export PLAYER_ENV=${PLAYER_ENV?Please set PLAYER_ENV}
export TIMEOUT_SERVER=${TIMEOUT_SERVER:-400}
export TIMEOUT_CLIENT=${TIMEOUT_CLIENT:-400}
export ISOLATE_TIME_LIMIT_SECS=${TIME_LIMIT_SECS:-350}
export ISOLATE_MEM_LIMIT_MIB=${ISOLATE_MEM_LIMIT_MIB:-500}
export ISOLATE_PROCESSES=${ISOLATE_PROCESSES:-100}

export WORKER_DEBUG=${WORKER_DEBUG:-}

WORKERNODE_CONFIG_PATH="/etc/prologin/workernode.yml"

tmp_config=$(mktemp workernode.XXXX)

envsubst < "$WORKERNODE_CONFIG_PATH" > "$tmp_config"
mv "$tmp_config" "$WORKERNODE_CONFIG_PATH"
chown runner:1000 "$WORKERNODE_CONFIG_PATH"

if [ -z "${WORKER_DEBUG}" ]; then
    su -c '/sadm/.venv/bin/python -m prologin.workernode -l' runner
else
    su -c '/sadm/.venv/bin/python -m prologin.workernode -l -v' runner
fi

