# NixOSADM

## IPs

### gates

10.224.33.3 gate-1
10.224.33.4 gate-2

10.224.33.245 floating IP for the gate. This is where ports are forwarded from
the CRI public IP.

Don't forget to disable hardware offloading on the NUCs

### k8s masters

10.224.33.5 master-1
10.224.33.6 master-2
10.224.33.7 master-3

10.224.33.248 floating IP for the API server. It should be a dns record.

### nfs server

10.224.33.242 nfs-2.

### concours/masternode

10.224.33.241

### IPs for services in k8s

10.224.33.249 and 10.224.33.254 are reserved for services running in k8s
10.224.33.249 is for ingress-nginx
10.224.33.250 is for ingress-nginx-internal

### random services

10.224.33.2 others-1. used for salt, dragonfly, etc.
