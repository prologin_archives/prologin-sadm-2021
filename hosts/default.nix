{ lib
, system
, pkgset
, self
, nixpkgs
, nixpkgs-master
, nixpie
, sops-nix
, impermanence
, ...
}@inputs:

let
  genAttrs' = func: values: builtins.listToAttrs (map func values);

  config = host: {
    name = host.name;
    value = lib.nixosSystem {
      inherit system;

      specialArgs = { inherit inputs; };

      modules =
        let
          core = self.nixosModules.profiles.core;

          global = {
            # force getting the hostname from dhcp
            networking.hostName = lib.mkForce "";
            nix.nixPath = [
              "nixpkgs=${nixpkgs}"
              "nixpkgs-master=${nixpkgs-master}"
            ];

            nixpkgs = { inherit (pkgset) pkgs; };

            nix.registry = {
              nixpkgs.flake = nixpkgs;
              nixpkgs-master.flake = nixpkgs-master;
              nixpie.flake = nixpie;
              sadm.flake = self;
            };

            system.configurationRevision = lib.mkIf (self ? rev) self.rev;
          };

          sops-fix = { config, pkgs, lib, ... }: {
            system.activationScripts.setup-secrets =
              let
                sops-install-secrets = sops-nix.packages.x86_64-linux.sops-install-secrets;
                manifest = builtins.toFile "manifest.json" (builtins.toJSON {
                  secrets = builtins.attrValues config.sops.secrets;
                  # Does this need to be configurable?
                  secretsMountPoint = "/run/secrets.d";
                  symlinkPath = "/run/secrets";
                  inherit (config.sops) gnupgHome sshKeyPaths;
                });

                checkedManifest = pkgs.runCommandNoCC "checked-manifest.json"
                  {
                    nativeBuildInputs = [ sops-install-secrets ];
                  } ''
                  sops-install-secrets -check-mode=${if config.sops.validateSopsFiles then "sopsfile" else "manifest"} ${manifest}
                  cp ${manifest} $out
                '';
              in
              lib.mkForce (lib.stringAfter [ "users" "groups" ] ''
                echo setting up secrets...
                export PATH=$PATH:${pkgs.gnupg}/bin:${pkgs.gnupg}/sbin
                SOPS_GPG_EXEC=${pkgs.gnupg}/bin/gpg ${sops-install-secrets}/bin/sops-install-secrets ${checkedManifest}
              '');
          };


          deploy = { inherit (host) deploy; };

          flakeModules = builtins.attrValues (removeAttrs self.nixosModules [ "profiles" "sadm" ]);
        in
        [
          impermanence.nixosModules.impermanence
          sops-nix.nixosModules.sops
          sops-fix
          nixpie.nixosModules.nixpie

          core
          global
          deploy
          (import host.confFile)
        ] ++ flakeModules;
    };
  };

  hosts = genAttrs' config [
    {
      name = "concours";
      confFile = ./concours/configuration.nix;
      deploy.hostname = "10.224.33.241";
    }
    {
      name = "others-1";
      confFile = ./others-1/configuration.nix;
      deploy.hostname = "10.224.33.2";
    }
    {
      name = "nfs-2";
      confFile = ./nfs-2/configuration.nix;
      deploy.hostname = "10.224.33.242";
    }
    {
      name = "gate-1";
      confFile = ./gate-1/configuration.nix;
      deploy.hostname = "10.224.33.3";
    }
    {
      name = "gate-2";
      confFile = ./gate-2/configuration.nix;
      deploy.hostname = "10.224.33.4";
    }
    {
      name = "master-1";
      confFile = ./master-1/configuration.nix;
      deploy = {
        hostname = "10.224.33.5";
        ip = "10.224.33.5";
      };
    }
    {
      name = "master-2";
      confFile = ./master-2/configuration.nix;
      deploy = {
        hostname = "10.224.33.6";
        ip = "10.224.33.6";
      };
    }
    {
      name = "master-3";
      confFile = ./master-3/configuration.nix;
      deploy = {
        hostname = "10.224.33.7";
        ip = "10.224.33.7";
      };
    }
  ];
in
hosts
