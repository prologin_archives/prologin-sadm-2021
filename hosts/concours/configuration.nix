{ inputs, lib, pkgs, ... }:

{
  imports = [
    inputs.self.nixosModules.profiles.server

    ./hardware-configuration.nix
  ];

  networking = {
    hostName = lib.mkOverride 10 "concours";
    useDHCP = false;
    nameservers = [ "10.224.4.2" ];

    bonds.bond0 = {
      interfaces = [ "enp3s0f0" "enp3s0f1" "enp4s0f0" "enp4s0f1" ];
      driverOptions = {
        mode = "balance-alb";
      };
    };

    interfaces.bond0 = {
      ipv4.addresses = [{
        address = "10.224.33.241";
        prefixLength = 24;
      }];
    };

    defaultGateway = {
      address = "10.224.33.1";
      interface = "bond0";
    };
  };

  users.users.root.openssh.authorizedKeys.keys = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINlovNp4ufIs0NcdyJqTjHmW2/Uj9QfS9hf/fX+/yphT rsnapshot@nfs-2"
  ];

  networking.firewall.enable = false; # FIXME

  virtualisation.docker = {
    enable = true;
    extraOptions = "--live-restore";
  };

  environment.systemPackages = with pkgs; [
    docker-compose
  ];

  services.postgresql = {
    enable = true;
    package = pkgs.postgresql_13;
    enableTCPIP = true;
    ensureDatabases = [ "concours" ];
    ensureUsers = [{
      name = "concours";
      ensurePermissions = {
        "DATABASE concours" = "ALL PRIVILEGES";
      };
    }];
  };

  services.postgresqlBackup = {
    enable = true;
    location = "/srv/backup";
    startAt = "*:*:32";
  };

  services.nginx = {
    enable = true;
    virtualHosts.concours = {
      default = true;
      serverName = "finale.prologin.dev";

      locations = {
        "/static/" = {
          root = "/srv/concours/static";
        };

        "@django" = {
          proxyPass = "http://localhost:8000";
          extraConfig = ''
            proxy_set_header Host 'finale.prologin.dev';
            proxy_set_header X-Forwarded-For $http_add_x_forwarded_for;
          '';
        };

        "/" = {
          tryFiles = "$uri @django";
        };
      };
    };

    virtualHosts.champions = {
      serverName = "champions.finale.prologin.dev";
      extraConfig = ''
        auth_basic "You won't get others' champions.";
        auth_basic_user_file /srv/champions_basic_auth;
      '';

      locations = {
        "/" = {
          root = "/srv/concours/shared";
          extraConfig = ''
            autoindex on;
          '';
        };
      };
    };

    virtualHosts.maps = {
      serverName = "maps.prologin.dev";
      locations = {
        "/" = {
          root = "/srv/concours/prologin2021/www";
          index = "editor.html";
        };

        "/static/" = {
          root = "/srv/concours/static";
        };
      };
    };
  };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "21.05"; # Did you read the comment?
}
