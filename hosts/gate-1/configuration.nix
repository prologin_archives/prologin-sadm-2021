{ inputs, lib, ... }:

{
  imports = [
    inputs.self.nixosModules.profiles.server
    inputs.self.nixosModules.profiles.nuc
    inputs.self.nixosModules.profiles.gate

    ./hardware-configuration.nix
  ];

  networking = {
    useDHCP = false;
    interfaces.eno1.useDHCP = true;
  };

  services.keepalived.vrrpInstances."VI_01".state = lib.mkForce "MASTER";
  services.keepalived.vrrpInstances."VI_01".priority = lib.mkForce 150;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "21.05"; # Did you read the comment?
}
