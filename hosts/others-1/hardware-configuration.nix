{ config, lib, pkgs, modulesPath, ... }:

{
  imports = [
    (modulesPath + "/installer/scan/not-detected.nix")
  ];

  boot = {
    loader = {
      systemd-boot.enable = true;
      efi.canTouchEfiVariables = true;
    };

    initrd = {
      availableKernelModules = [
        "ahci"
        "nvme"
        "rtsx_pci_sdmmc"
        "sd_mod"
        "usb_storage"
        "usbhid"
        "xhci_pci"
      ];
    };

    kernelModules = [ "kvm-intel" ];
  };

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-uuid/e064ae84-0e81-4e6c-af6a-a7242c79bec4";
      fsType = "ext4";
    };

    "/boot" = {
      device = "/dev/disk/by-uuid/F565-2A19";
      fsType = "vfat";
    };
  };

  swapDevices = [
    { device = "/dev/disk/by-uuid/9443e392-213d-49ee-a4c8-f4369c943d1b"; }
  ];

  powerManagement.cpuFreqGovernor = "performance";
}
