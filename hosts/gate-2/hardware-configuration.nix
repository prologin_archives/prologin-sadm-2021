{ config, lib, pkgs, modulesPath, ... }:

{
  imports = [
    (modulesPath + "/installer/scan/not-detected.nix")
  ];

  boot = {
    loader = {
      systemd-boot.enable = true;
      efi.canTouchEfiVariables = true;
    };

    initrd = {
      availableKernelModules = [
        "ahci"
        "nvme"
        "rtsx_pci_sdmmc"
        "sd_mod"
        "usb_storage"
        "usbhid"
        "xhci_pci"
      ];
    };

    kernelModules = [ "kvm-intel" ];
  };

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-uuid/95f1dc1e-1be2-441f-b0d6-9965f607e7b5";
      fsType = "ext4";
    };

    "/boot" = {
      device = "/dev/disk/by-uuid/058A-4C9A";
      fsType = "vfat";
    };
  };

  swapDevices = [
    { device = "/dev/disk/by-uuid/f975a0b3-d7c9-45ee-9e25-990970eeeeb2"; }
  ];

  powerManagement.cpuFreqGovernor = "performance";
}
