{ pkgs, config, ... }:

{
  services.keepalived = {
    enable = true;
    vrrpScripts = {
      "check_haproxy" = {
        script = "${pkgs.killall}/bin/killall -0 haproxy";
        interval = 3;
        user = "root";
      };
    };
    vrrpInstances."VI_01" = {
      state = "BACKUP";
      interface = "eno1";
      virtualRouterId = 245;
      noPreempt = true;
      priority = 100;
      virtualIps = [{
        addr = "10.224.33.245";
      }];
      trackScripts = [ "check_haproxy" ];
      trackInterfaces = [ "eno1" ];
    };
  };

  networking.firewall.extraCommands = ''
    iptables -A INPUT -p vrrp -j ACCEPT
  '';

  networking.firewall.extraStopCommands = ''
    iptables -D INPUT -p vrrp -j ACCEPT
  '';

  networking.firewall.allowedTCPPorts = [ 80 443 6443 25565 25566 25567 ];

  boot.kernel.sysctl."net.ipv4.ip_nonlocal_bind" = true;

  services.haproxy = {
    enable = true;
    config = ''
        # global configuration is above
        log /dev/log local0
        log /dev/log local1 notice

      defaults
        mode tcp
        log global
        option tcplog
        option dontlognull
        option forwardfor except 127.0.0.0/8
        option redispatch
        retries 1
        timeout queue 20s
        timeout connect 5s
        timeout client 20s
        timeout server 20s
        timeout check 10s

      backend api-server
        mode tcp
        server apiserver-floating 10.224.33.248:6443
      frontend api-server
        bind 10.224.33.245:6443
        use_backend api-server

      backend minecraft-survival
        mode tcp
        server minecraft-floating 10.224.33.249:25565
      frontend minecraft-survival
        bind 10.224.33.245:25565
        use_backend minecraft-survival

      backend minecraft-minigames
        mode tcp
        server minecraft-floating 10.224.33.249:25566
      frontend minecraft-minigames
        bind 10.224.33.245:25566
        use_backend minecraft-minigames

      backend minecraft-creative
        mode tcp
        server minecraft-floating 10.224.33.249:25567
      frontend minecraft-creative
        bind 10.224.33.245:25567
        use_backend minecraft-creative

      backend web80
        mode tcp
        server web-floating 10.224.33.249:80
      backend web443
        mode tcp
        server web-floating 10.224.33.249:443
      frontend web80
        bind 10.224.33.245:80
        use_backend web80
      frontend web443
        bind 10.224.33.245:443
        use_backend web443
    '';
  };
}
