{ config, pkgs, lib, ... }:

with lib;

let
  cfg = config.services.k3s;
in
{
  imports = [
    ./etcd.nix
    ./haproxy.nix
    ./ragec.nix
  ];

  environment.persistence."/srv" = {
    directories = mkForce [ ];
  };

  sops.secrets =
    let
      commonSopsOptions = {
        sopsFile = ../certs/certs.yml;
      };
    in
    {
      client-master-1-key = commonSopsOptions;
      client-master-2-key = commonSopsOptions;
      client-master-3-key = commonSopsOptions;

      k3s_token.sopsFile = ../certs/k3s-token.yml;
    };

  environment.systemPackages = [ pkgs.kubectl ];
  environment.variables.KUBECONFIG = "/var/lib/rancher/k3s/server/cred/admin.kubeconfig";

  networking.firewall.allowedTCPPorts = [
    6443 # API server
  ];

  services.k3s = {
    role = "server";
    disableAgent = false;
  };

  systemd.services.k3s.serviceConfig.ExecStart = mkForce (pkgs.writeShellScript "k3s.sh" ''
    exec ${cfg.package}/bin/k3s ${cfg.role} \
      ${optionalString cfg.docker "--docker"} \
      ${optionalString cfg.disableAgent "--disable-agent"} \
      --server ${cfg.serverAddr} \
      --node-taint CriticalAddonsOnly=true:NoExecute \
      --tls-san 10.224.33.248 \
      --tls-san apiserver.k8s.finale.devou.ps \
      --tls-san apiserver.k8s.prologin.dev \
      --token-file ${config.sops.secrets.k3s_token.path} \
      --datastore-endpoint https://10.224.33.5:2379,https://10.224.33.6:2379,https://10.224.33.7:2379 \
      --datastore-cafile ${../certs/ca.pem} \
      --datastore-certfile ${../certs}/client-${config.services.etcd.name}.pem \
      --datastore-keyfile ${config.sops.secrets."client-${config.services.etcd.name}-key".path} \
      --bind-address ${config.deploy.ip} \
      --disable-network-policy \
      --cluster-cidr 192.168.0.0/16 \
      --service-cidr 172.31.0.0/16 \
      --disable coredns \
      --disable servicelb \
      --disable traefik \
      --disable local-storage \
      --flannel-backend=none \
      --pause-image registry.gitlab.com/risson/prologin-sadm-2021/rancher/pause:3.1
  '');
}
