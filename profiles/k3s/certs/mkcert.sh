#! /usr/bin/env bash

set -xeuo pipefail

# CA
cfssl genkey -initca ./config/csrCA.json | \
  cfssljson -bare ./ca

cat <<EOF > certs.yml
ca_key: |
$(cat ./ca-key.pem | sed 's/^/  /')
EOF

for i in $(seq 1 3); do
  cfssl gencert \
      -ca ./ca.pem \
      -ca-key ./ca-key.pem \
      -config ./config/ca-config.json \
      ./config/etcd.json | \
    cfssljson -bare ./master-${i}
  cat <<EOF >> certs.yml
master-${i}-key: |
$(cat ./master-${i}-key.pem | sed 's/^/  /')
EOF
  rm ./master-${i}-key.pem

  cfssl gencert \
      -ca ./ca.pem \
      -ca-key ./ca-key.pem \
      -config ./config/ca-config.json \
      ./config/etcd.json | \
    cfssljson -bare ./peer-master-${i}
  cat <<EOF >> certs.yml
peer-master-${i}-key: |
$(cat ./peer-master-${i}-key.pem | sed 's/^/  /')
EOF
  rm ./peer-master-${i}-key.pem

  cfssl gencert \
      -ca ./ca.pem \
      -ca-key ./ca-key.pem \
      -config ./config/ca-config.json \
      ./config/etcdClient.json | \
    cfssljson -bare ./client-master-${i}
  cat <<EOF >> certs.yml
client-master-${i}-key: |
$(cat ./client-master-${i}-key.pem | sed 's/^/  /')
EOF
  rm ./client-master-${i}-key.pem
done

sops --encrypt --in-place ./certs.yml && rm ./ca-key.pem ./*.csr

cat <<EOF > k3s-token.yml
k3s_token: "$(head -c 16 /dev/urandom | od -An -t x | tr -d ' ')"
EOF
sops --encrypt --in-place ./k3s-token.yml
